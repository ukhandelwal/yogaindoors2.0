var express = require('express');
var app = express();
var bcryptnodejs = require('bcrypt-nodejs');
var dbhandler = require('./persistencelayer/dbhandler.js');
var emailer = require('./emailer.js');
var path = require('path');

app.use("/public", express.static(path.join(__dirname, 'public')));
// for file reading operations.
var filereader = require('fs');
// adding body parser to parse json objects.
var parser = require("body-parser");
app.use(parser.urlencoded({ extended: true }));
app.use(parser.json());

var session = require('express-session');
app.use(session({ secret: 'annhud',
    saveUninitialized: true,
    resave: true
}));
var sess;

app.get('/', function (req, res) {
    sess = req.session;
    console.log("session email " + sess.email);
    redirectHomePage(sess, res);

});

function redirectHomePage(sess, res) {
    if (typeof sess === 'undefined' || typeof sess.email === 'undefined') {
        var jsonData = {
            sessiondata: ""
        }
        res.render("main/index.ejs", jsonData);
    } else {
        var jsonData = {
            sessiondata: sess.email
        }
        res.render("main/index.ejs", jsonData);
    }
}

app.use(express.static(__dirname + '/public'))
app.listen(11262);

// ejs inclusion.
var ejs = require('ejs');
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

require('./features/linkredirects.js').redirectUrls(app);
require('./features/signinuser.js').signinUser(app, dbhandler, bcryptnodejs);
require('./features/signupuser.js').signUpUser(app, dbhandler, bcryptnodejs, emailer);

app.get('/signout', function (req, res) {
    sess = req.session;
    sess.email = undefined;
    redirectHomePage(sess, res);
});

app.post('/forgotpassword', function (req, res) {
    var username = req.body.forgotPassword.email;
    dbhandler.findUserInfo(username, function (err, rows, fields) {
        if (err) {
            var jsonData = {
                type: 0,
                string: "Error in retrieving data! Try again"
            }
        } else {
            if (rows.length == 0) {
                var jsonData = {
                    type: 0,
                    string: "The email id is not registered."
                }
                res.type('json');
                res.send(jsonData);
            } else {
                var randomstring = Math.random().toString(36).slice(-8);
                var cryptPassword = bcryptnodejs.hashSync(randomstring);
                dbhandler.changePassword(username, cryptPassword, function (err, result) {
                    var jsonData = {
                        type: 1,
                        string: "An email has been sent to your registered id with the new password. Use the same to login"
                    }
                    res.type('json');
                    res.send(jsonData);
                });

            }
        }

    });
});