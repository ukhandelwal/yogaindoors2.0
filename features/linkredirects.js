exports.redirectUrls = function (app) {
    app.get('/js/:id', function (req, res) {
        console.log("Entering this for some purpose i say " + __dirname);
        res.sendFile(req.params.id, { root: __dirname + '/../views/js' });
    });

    app.get('/font-awesome/css/:id', function (req, res) {
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/css' });
    });

    app.get('/font-awesome/fonts/:id', function (req, res) {
        console.log(req.params.id);
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/fonts' });
    });

    app.get('/font-awesome/less/:id', function (req, res) {
        console.log(req.params.id);
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/less' });
    });

    app.get('/font-awesome/scss/:id', function (req, res) {
        console.log(req.params.id);
        res.sendFile(req.params.id, { root: __dirname + '/../views/font-awesome/scss' });
    });

    app.get('/userprofile', function (req, res) {
        res.render('userportal/userprofile.ejs');
    });
}