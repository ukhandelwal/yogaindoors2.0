exports.signinUser = function (app, dbhandler, bcryptnodejs) {
    app.post('/signin', function (req, res) {
        var username = req.body.signin.email;
        var password = req.body.signin.password;
        console.log("the registered username " + username);
        dbhandler.findUserInfo(username, function (err, rows, fields) {
            if (err) {
                var jsonData = {
                    type: 0,
                    string: 'error in retrieving data'
                }
            } else {
                if (rows.length == 0) {
                    var jsonData = {
                        type: 0,
                        string: 'The email is not registered'
                    }
                } else {
                    if (rows[0].verify != 1) {
                        var jsonData = {
                            type: 0,
                            string: 'The email is not verified'
                        }
                    } else {
                        if (bcryptnodejs.compareSync(password, rows[0].password)) {
                            var jsonData = {
                                type: 1,
                                string: 'Login successful'
                            }
                            sess = req.session;
                            sess.email = username;
                            console.log(sess.email);
                        } else {
                            var jsonData = {
                                type: 0,
                                string: 'The username and password do not match'
                            }
                        }
                    }
                }
            }
            res.type('json');
            res.send(jsonData);
        });
    });
}