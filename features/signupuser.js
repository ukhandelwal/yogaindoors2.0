exports.signUpUser = function(app, dbhandler,bcryptnodejs,emailer){
    app.post('/signup', function (req, res) {
    var username = req.body.signup.username;
    var password = req.body.signup.password;
    dbhandler.findUserInfo(username, function (err, rows, fields) {
        if (err) {
            console.log(err);
            res.sendStatus(404);
        } else {
            if (rows.length == 0) {
                var cryptPassword = bcryptnodejs.hashSync(password);
                var generateRandomString = Math.floor((Math.random() * 1234) + 909);
                console.log(cryptPassword + " C_G " + generateRandomString);
                dbhandler.registerUser(username, cryptPassword, generateRandomString, function (err, row) {
                    var link = "http://" + req.headers.host + "/verify/" + generateRandomString;
                    res.type('json');
                    //res.writeHead(200, { 'Content-Type': 'application/json' });
                    var jsonData = {
                        type: 1,
                        string: 'A verification mail has been sent to the user.'
                    }
                    emailer.sendVerifyEmail(username, link, function (err, json) {
                        res.send(jsonData);
                    });
                });
            } else {
                var jsonData = {
                    type: 0,
                    string: 'user is already registered'
                }
                console.log('user is already registered');
                res.type('json');
                res.send(jsonData);
            }

        }
    });
});

app.get('/verify/:id', function (req, res) {
    var verify = req.params.id;
    dbhandler.verifyMail(verify, function (err, result) {
        console.log(err);
        console.log(result);
        res.sendStatus(200);
    });
});
}